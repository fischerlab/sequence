#!/usr/local/bin/python -u
import re
from sequence import fasta as F
import cPickle as P
from glob import glob
import os
import time



class UltimateIterativeScreen(object):
    '''
    '''
    def __init__ (self,inPath=None,cushion=15,debug=False):
        """
        """

        self.cushion=cushion
        self.debug=debug
        self.longestRemaining=[]
        self.cycleNumber=0
        self.debugDict={}
        
        if inPath != None:
            self.setup(inPath)


    def setup(self,sliceNumber):
        """
        """
        self.reportDict,self.blastDict=prepareSlice(sliceNumber,cushion=self.cushion)
        self.longestRemaining=findLongest(self.reportDict)

        return self.longestRemaining[-1][-1]

        self.stderr=file(self.localPath + '.ULTstderr','w')
        self.stdout=file(self.localPath + '.ULTstdout','w')


    def screen(self,hitToBeScreened):
        """screen and return the longest sequence after screening
        """
        if hitToBeScreened in self.reportDict:
            del self.reportDict[hitToBeScreened]
        if hitToBeScreened in self.blastDict:
            for seq in self.blastDict[hitToBeScreened].iterkeys():
                if seq in self.reportDict:
                    self.reportDict[seq]=numberScreener(self.reportDict[seq],self.blastDict[hitToBeScreened][seq],cushion=self.cushion)
                    if self.reportDict[seq][-1]<1:
                        del self.reportDict[seq]
        mid3=time.time()
            

        self.longestRemaining=findLongest(self.reportDict)
        mid4=time.time()
        self.debugDict['time']='timing: %s'%(mid4-mid3)
        #return self.debugDict['time']
        return self.longestRemaining[-1][-1]

class SeqRegions(dict):
    '''dict of SeqTitles:Regions,length
    '''
    #def longestRemaining(self):
    #    return sorted(self.items(),key=lambdaX: X[-1][-1])[-1][0]

class Regions(object):
    '''
    '''
    def __init__(self):
        self.regionList=[]
        self.baseCount=0

    def addRegions(self,ranges):
        for s,e in ranges:
            self.regionList.append((s,e))
            self.baseCount+=e-s+1
        self.regionList.sorted()





def prepareSlice(sliceNumber,cushion=15):
    '''prepares the 2 dictionaries for a given slice.
    The first dictionary contains the reports for all slice sequences.
    The second dictionary contains the blast results for all
    slice sequences against all other sequences.
    '''
    
    fastaName='slice%s.fasta'%sliceNumber
    reportDict=fastaParser(fastaName,cushion=cushion)
    print 'first dictionary complete, %s sequences stored'%len(reportDict)

    i=0
    blastDict={}
    for f in glob('%s_*.pkl'%sliceNumber):
        i+=1
        print 'parsing pickle %s'%i
        blastDict.update(P.load(file(f)))
        print 'Dictionary length: %s'%len(blastDict)
        
    return reportDict,blastDict
        
    
def findLongest(reportDict):
    '''returns the name of the longest record
    in the sequence report dictionary
    '''
    ## if len(reportDict)>0:
    ##     #k,v=sorted(reportDict.items(),key=lambda X: X[-1][-1])[-1]
    ##     k,v=reportDict.items().sort(key=lambda X: X[-1][-1])[-1]
    ##     rv=[k,v]
    ## else:
    ##     rv=['',[0]]
    try:
        rv=max(((v[-1],k) for k,v in reportDict.iteritems()))    
        return [rv[-1],reportDict[rv[-1]]]
    except ValueError:
        return ['',[0]]

    
def parse(iFile, cushion=15):
    '''
    '''
    iFile=file(iFile)
    totalHitDict={}
    totalQueryDict={}
    store=True
    i=0
    for l in iFile:
        i+=1
        if i==1000000:
            print i
        if i%10000000==0:
            print i
        if l.startswith('      <Iteration_query-def>'):
            queryDef=l.split('>')[1].split('<')[0]
        if l.startswith('          <Hit_def>'):
            hitDef=l.split('>')[1].split('<')[0]
        if l.startswith('              <Hsp_query-from>'):
            queryStart=int(l.split('>')[1].split('<')[0])
        if l.startswith('              <Hsp_query-to>'):
            queryEnd=int(l.split('>')[1].split('<')[0])
        if l.startswith('              <Hsp_hit-from>'):
            hitStart=int(l.split('>')[1].split('<')[0])
        if l.startswith('              <Hsp_hit-to>'):
            hitEnd=int(l.split('>')[1].split('<')[0])
        if l.startswith('              <Hsp_qseq>'):
            qSeq=l.split('>')[1].split('<')[0]
        if l.startswith('              <Hsp_hseq>'):
            hSeq=l.split('>')[1].split('<')[0]
        if l.startswith('              <Hsp_midline>'):
            queryIndices,hitIndices=midline(l,queryStart,queryEnd,hitStart,hitEnd,qSeq,hSeq,cushion=cushion)
            #hitrange is a list of tuples that are the range(s) to be stored
            if len(queryIndices)==0:
                store=False
        

        if l.startswith('            </Hsp>'):
            if store==False:
                store=True
                continue
            else:
                if queryDef in totalHitDict:
                    if hitDef in totalHitDict[queryDef]:
                        totalHitDict[queryDef][hitDef].extend(hitIndices)
                    else:
                        totalHitDict[queryDef][hitDef]=[]
                        totalHitDict[queryDef][hitDef].extend(hitIndices)
                else:
                    totalHitDict[queryDef]={}
                    totalHitDict[queryDef][hitDef]=[]
                    totalHitDict[queryDef][hitDef].extend(hitIndices)
                if hitDef in totalQueryDict:
                    if queryDef in totalQueryDict[hitDef]:
                        totalQueryDict[hitDef][queryDef].extend(queryIndices)
                    else:
                        totalQueryDict[hitDef][queryDef]=[]
                        totalQueryDict[hitDef][queryDef].extend(queryIndices)
                else:
                    totalQueryDict[hitDef]={}
                    totalQueryDict[hitDef][queryDef]=[]
                    totalQueryDict[hitDef][queryDef].extend(queryIndices)
    return totalHitDict,totalQueryDict
    


def midline(l,queryStart,queryEnd,hitStart,hitEnd,qSeq,hSeq,cushion=15):
    '''takes the midline and associated ranges, spits out the exact ranges
    we want to screen out, in the form of a list of tuples
    '''
    l=l.split('>')[1].split('<')[0]
    rexp=re.compile(r'\|+')
    alignment=l
    indicesRaw=[(m.start()+1,m.end()) for m in rexp.finditer(alignment)]
    gexp=re.compile(r'\-+')
    queryGap=[m.end() for m in gexp.finditer(qSeq)]
    hitGap=[m.end() for m in gexp.finditer(hSeq)]
    queryIndicesRaw=[]
    hitIndicesRaw=[]
    queryIndices=[]
    hitIndices=[]
    for s,e in indicesRaw:
        s+=cushion
        e+=-cushion
        if s>e:
            continue
        if len(queryGap)>0:
            x=0
            for g in queryGap:
                if g<s:
                    x+=1
            queryIndicesRaw.append((s-x,e-x))
        else:
            queryIndicesRaw.append((s,e))
        if len(hitGap)>0:
            x=0
            for g in hitGap:
                if g<s:
                    x+=1
            hitIndicesRaw.append((s-x,e-x))
        else:            
            hitIndicesRaw.append((s,e))
        
    for s,e in queryIndicesRaw:
        if queryEnd>queryStart:
            queryIndices.append((queryStart+s-1,queryStart+e-1))
        else:
            queryIndices.append((queryStart-e+1,queryStart-s+1))
            
    for s,e in hitIndicesRaw:            
        if hitEnd>hitStart:
            hitIndices.append((hitStart+s-1,hitStart+e-1))
        else:
            hitIndices.append((hitStart-e+1,hitStart-s+1))

    return queryIndices,hitIndices


def sequenceReport(s,cushion=15):
    '''Returns a list of tuples, the tuples being valid DRNA regions
    in the sequence s. Also screens out segments that are smaller than
    twice the cushion size
    '''
    rexp=re.compile(r'[ACGTUacgtu]+')
    indices=[(m.start()+1,m.end()) for m in rexp.finditer(s)]
    length=0
    indicesToRemove=[]
    for c in indices:
        stretch=(c[1]-c[0]+1)
        if stretch-1<cushion*2:
            indicesToRemove.append(c)
        else:
            length+=stretch
    for r in indicesToRemove:
        indices.remove(r)
    indices.append(length)
    return indices

def numberScreener(report,hits,cushion=15):
    '''screens the bases in a sequence report according to the hits presented
    Also drops newly created shorties and adjusts sequence length
    '''
    length=report[-1]
    report.remove(length)
    screen=False
    for s,e in hits:
        i=-1
        for s2,e2 in report:
            i+=1
            if e<=e2 and s>=s2:
                screen=True
                break
            if s>=s2 and s<=e2:
                screen=True
                break
            if e<=e2 and e>=s2:
                screen=True
                break
            if e>=e2 and s<=s2:
                screen=True
                break
        if screen:
            screen=False
            report.remove((s2,e2))
            length-=(e2-s2+1)
            #remove old segment, adjust length
            #s=75,e=150,s2=50,e2=100
            new1=(e+1,e2)
            new2=(s2,s-1)
            #calculate new segments
            if new1[1]-new1[0]+1>cushion*2:
                report.insert(i,new1)
                length+=(new1[1]-new1[0]+1)
            if new2[1]-new2[0]+1>cushion*2:
                report.insert(i,new2)
                length+=(new2[1]-new2[0]+1)
            #add new segments if they are long enough, extend length
        else:
            print 'DID NOT FIND MATCH'
        
    report.append(length)
    return report

def fastaParser(iFile,cushion=15):
    '''uses FastaIterator to parse fasta file into dictionary with
    valid ranges and sequence length
    '''
    rv={}
    i=0
    for rec in F.FastaIterator(iFile):
        rv[rec.title]=sequenceReport(rec.sequence,cushion=cushion)
    return rv


def pickleDumper(cushion=15):
    '''
    '''
    i=0
    for f in glob('testBlast*.out'):
        i+=1
        print 'making pickle #%s'%i
        hd,qd=parse(f,cushion=cushion)
        hdName=f.split('Blast')[1].split('.')[0][0]
        qdName=f.split('Blast')[1].split('.')[0][1]

        oFile=file('%s_%s.pkl'%(hdName,qdName),'w')
        P.dump(hd,oFile)
        oFile.close()

        oFile=file('%s_%s.pkl'%(qdName,hdName),'w')
        P.dump(qd,oFile)
        oFile.close()
        del hd,qd

def blastRunner():
    '''
    '''
    i=0
    run=[]
    for f in glob('slice*.fasta'):
        i+=1
        print 'running blasts for slice %s'%i
        fName=f.split('.')[0][-1]
        ii=0
        for f2 in glob('slice*.fasta'):
            ii+=1
            f2Name=f2.split('.')[0][-1]
            if f2 in run:
                continue
            else:
                print 'sub blast %s'%ii
                os.system('gridIt.py "megablast -W24 -D2 -m7 -FF -R -d %s -i %s"'%(f,f2))
                #os.system('megablast -W24 -D2 -m7 -FF -R -d %s -i %s > testBlast%s.out'%(f,f2,fName+f2Name))

        run.append(f)

def resultsTranslator(resultFile,sliceList):
    '''
    '''


def dictTester(a,b):
    '''tests various differences between two dictionaries
    '''
    ta=0
    ia=0
    iia=0
    for k in a.iterkeys():
        ia+=1
        for k2 in a[k].iterkeys():
            iia+=1
            ta+=len(a[k][k2])

    tb=0
    ib=0
    iib=0
    for k in b.iterkeys():
        ib+=1
        for k2 in b[k].iterkeys():
            iib+=1
            tb+=len(b[k][k2])

    print ia,iia,ta,ib,iib,tb

def dictTester2(a,b):
    '''tests various differences between two dictionaries
    '''
    for k in a.iterkeys():
        for k2 in a[k].iterkeys():
            if len(a[k][k2])!=len(b[k2][k]):
                print 'ding'
                break
            
