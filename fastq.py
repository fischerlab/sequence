"""fastq
sequence utilities

kael and dale

$Id: fastq.py,v 1.11 2011/05/03 21:07:11 julio Exp $

"""
__version__ ="$Revision: 1.11 $"

import commands

import fasta
from __init__ import *
from utils import flatten, fileIterator, getIteratable, safeOFW

FASTQ_STRING_FMT = "@%s\n%s+\n%s"


#
# Fastq Record Class incorporating quality values.
# Built on top of the fasta.Record class.
#
class Record (fasta.Record):
    """The Fastq Record class.
    
    Members:
    title       Title line ('@' character not included).
    sequence    The sequence.
    quality     The quality values.

    Record acts like a string in these cases:
    len(rec)
    rec[x:y]

    str(rec) returns a valid qfastq formatted string

    """

    def __init__ (self,title='',sequence='',colwidth=60,quality=None):
        """Create a new Record.  colwidth is deprecated and ignored. string
        output is always 4 lines and unwrapped.  Quality is an array of
        integers representing the quality of each base, or None.
        """
        fasta.Record.__init__(self,title=title,sequence=sequence,colwidth=colwidth)
        if quality:
            self.quality=quality
        else:
            self.quality=[]

    def __str__(self):
        s = []
        s.extend(('@%s' % self.title,self.sequence,'+',
                  "".join( intToQual( self.quality))))
        return "\n".join(s)

    def __getitem__(self,item):
        return (self.sequence[item], self.quality[item])
   
    def split(self,sliceBases=100000):
        """Returns an iterator of slices of the record
        """
        n=0
        for start in range(0,len(self.sequence),sliceBases):
            rec = Record()
            rec.title = self.title 
            rec.sequence = self.sequence[start:start+sliceBases]
            rec.quality = self.quality[start:start+sliceBases]
            rec.slice=n
            yield rec
            n+=1

    def fasta( self ):
        """Returns a fasta.Record version of this fastq.Record.
        Basically removes the quality scores but allows you
        to use the fasta.Record specific functions."""

        return fasta.Record( title=self.title,sequence=self.sequence)

    def prune( self, quality, harsh=False ):
        """Prunes bases from the 3' end of this read until a
        base with 'quality' or higher quality score is found.
        Returns a pruned version of this record. Does not modify
        this Record.

        Setting harsh = True will start from the 5' end, and prune
        everything after the first base that has lower quality than
        the specified value."""

        if not harsh:
            seq = self.sequence
            qual = self.quality
            while len(qual) > 0 and qual[-1] < quality:
                qual = qual[:-1]
                seq = seq[:-1]
        else:
            seq = ""
            qual = []
            for i in range(len(self.sequence)):
                if self.quality[i] >= quality:
                    seq += self.sequence[i]
                    qual.append( self.quality[i] )
                else:
                    break

        return Record(title=self.title,
                      sequence=seq,
                      quality=qual )

    def trimEnds(self,quality):
        """Prunes bases from the ends of this read until a
        base with 'quality' or higher quality score is found.
        Moodify this Record."""
        for i,q in enumerate(self.quality):
            if q>=quality:
                if i>0:
                    self.quality=self.quality[i:]
                    self.sequence=self.sequence[i:]
                break
        
        for i,q in enumerate(reversed(self.quality)):   
            if q>=quality:
                if i>0:
                    self.quality=self.quality[:(-1*i)]
                    self.sequence=self.sequence[:(-1*i)]
                break
        
    

    def lowQ2N(self,quality,screenChar='N'):
        """replace low quality bases with N (or other screenChar).
        returns number of screened bases.
        """
        ct=0
        sChars=list(self.sequence)
        for i,c in enumerate(sChars):
            if self.quality[i] < quality:
                ct+=1
                sChars[i]=screenChar[0]
        self.sequence=''.join(sChars)
        return ct

    def minQ (self):
        """return minimum quality value
        """
        return min(self.quality)

    def maxQ (self):
        """return maximum quality value
        """
        return max(self.quality)

    def meanQ (self):
        """return mean quality value
        """
        return float(sum(self.quality))/float(len(self.quality))

    def medianQ (self):
        """return median quality value
        """
        import pylab 
        return pylab.median(self.quality)

def qualToInt( quals, offset=33):
    """Given one or more quality characters, returns the corresponding
    list of integer values, as defined by Solexa.
    """
    return map( lambda x: ord(x)-offset, quals )

def intToQual( ints,offset=33 ):
    """Given a list of integers, returns the corresponding
    quality string as defined by Solexa."""
    return "".join( map( lambda x: chr(x+offset), ints ) )

#
# Counter
#
def fastqCount(things):
    """Count the number of fastq records in a file-like things.

    file like things can be one of more (in a list of tuple)
    file objects or paths .
    """
    
    if type(things) in (ListType,TupleType):
        return map(fastqCount,things)
    if type(things) in StringTypes:
        things = open(things)
    c = 0
    chunkSize = 10**4  # lowered - no big change in speed from 10^9.[kf]
    chunk = things.read(chunkSize)
    titlePattern=re.compile(u'^\+$')
    while len(chunk) > 0:
        c += len(titlePattern.findall(chunk))
        chunk = things.read(chunkSize)
    return c





#
# Identifier
# 
def looksLikeFastq( path ):
    """Returns true if the given file handle appears to be a Fastq file.
    DOES NOT VALIDATE THE FILE. JUST CHECKS THE FIRST RECORD TO SEE IF
    IT LOOKS LIKE A FASTQ RECORD."""
    try:
        for record in FastqIterator(path): 
            return True
    except:
        return False
    
#
# Iterator
#

def FastqIterator(files,raw=False,titleSet=None):
    """return an iterator of Records found in file handle, fh.
    if records are not needed raw can be set to True, and then 
    you can get (titleStr, seqStr, qualityStr).  With raw output,
    the sequence and quality strings have the newlines still in them.
    """
    def readTotitle(fh, titleChar):
        """returns a tuple ([lines before the next title line], next tile line)
        """
        preLines = []
        while True:
            l = fh.readline()
            if l.startswith(titleChar):
                return (preLines,l)
            elif l == '':
                return preLines,None
            else:
                preLines.append(l)

    
    for fh in fileIterator(files):
        preLines,nextTitleLine =readTotitle(fh,'@')

        while nextTitleLine != None:
            seqTitle = nextTitleLine[1:].rstrip()
            preLines,nextTitleLine=readTotitle(fh,'+')
            qualTitle = nextTitleLine[1:].rstrip()
            if len(qualTitle.strip()) > 0 and seqTitle != qualTitle:
                raise FastqParseError, (
                    "Error in parsing: @title sequence entry must be immediately "
                    "followed by corresponding +title quality entry.")
            seqLines = preLines
            qualLines = []
            for i in range(len(seqLines)): # Quality characters should be the same length as the sequence
                qualLines.append( fh.readline() )

            preLines,nextTitleLine=readTotitle(fh,'@')

            if titleSet!= None and seqTitle not in titleSetSet:
                continue

            seqLines = map(lambda x: x.strip(), seqLines)
            qualLines = map(lambda x: x.strip(), qualLines)
            if raw:
                yield (seqTitle, ''.join(seqLines), ''.join(qualLines))
            else:
                rec=Record()
                rec.title=seqTitle
                rec.sequence=''.join(seqLines)
                rec.quality=flatten(map(lambda x: qualToInt(x,self.qualOffset),
                                        qualLines))
                yield rec

iterator=FastqIterator


def MultiSyncIterator(*fastq_files,**kw_args):
    """An iterator that returns a tuple of fastq records,
    or a tuples of (title,seq,quality string).

    Assumes that each record is 4 lines, which is not the spec. but it the
    only workable standard practice.

    Use raw=True to increase speed and return tuples:
    (tile, sequence, quality_string).  Otherwise Records
    are returned.

    :param fastq_files, raw=False:
    :return: iterator of tuples
    """

    fastq_fhs = [file(x) for x in fastq_files]
    while True:
        titles = [x.next().strip('@ \t\n') for x in fastq_fhs]
        sequences = [x.next().strip('@ \t\n') for x in fastq_fhs]
        [x.next().strip('@ \t\n') for x in fastq_fhs] # skip '+' lines
        qualities = [x.next().strip('@ \t\n') for x in fastq_fhs]

        if 'raw' in kw_args and kw_args['raw']:
            yield tuple(zip(titles,sequences,qualities))
        else:
            integer_quals= [tuple(qualToInt(string_qual))
                            for string_qual in qualities]
            yield tuple([Record(title=t[0],sequence=t[1],quality=t[2]) for t in
                             zip(titles,sequences,integer_quals)])



def phdQualIterator(fastaFiles, qualFiles, raw=False):
    def readTotitle(fh):
        """returns a tuple ([lines before the next title line], next tile line)
        """
        preLines = []
        while True:
            l = fh.readline()
            if l.startswith('>'):
                return (preLines,l)
            elif l == '':
                return preLines,None
            else:
                preLines.append(l)

    def qualityIterator(filename):
        fh = file(filename)
        preLines, nextTitleLine = readTotitle(fh)

        while nextTitleLine != None:
            title = nextTitleLine[1:].rstrip()
            preLines, nextTitleLine = readTotitle(fh)

            yield (title, ' '.join(preLines))

    qualFiles = getIteratable(qualFiles)
    for idx, fastaFh in enumerate(fileIterator(fastaFiles)):
        qualIter = qualityIterator(qualFiles[idx])

        for seqTitle, sequence in fasta.FastaIterator(fastaFh, raw=True):
            qTitle, qualities = qualIter.next()
            sequence = sequence.replace('\n', '')
            qualities = qualities.replace('\n', '')
            if raw:
                yield (seqTitle, ''.join(sequence), ''.join(qualities))
            else:
                qualities = qualities.split()
                if len(sequence) != len(qualities):
                    raise Exception, 'Invalid number of qualities'
                rec = Record()
                rec.title = seqTitle
                rec.sequence = sequence
                rec.quality = qualities
                yield rec
            
def fastqCount(filename):
    grep_c = commands.getoutput("grep -c '^+$' %s" % filename)
    return int(grep_c)


class SplitMiSeqSet(object):
    """using a mapping of: (i7, i5 barcodes): sample_name,..., takes set of 4 MiSeq fastq
    files (R1, i7, i5, R2) and splits them into fastq files sample1_R1.fastq, sample1_R2.fastq,
    etc for n samples.
    """

    def __init__(self, sample_map, r1_fastq, i7_fastq, i5_fastq, r2_fastq,
                 max_mismatches_per_index=1, clobber=False):
        """Takes fastq file paths sample/barcode mapping dictionary: keys= (i7,i5), values = sample
        name,
        file paths,
        and a number of mismatches allowed in an index match.
        """
        self.sample_map=sample_map
        self.iterator = MultiSyncIterator(r1_fastq, i7_fastq, i5_fastq, r2_fastq, raw=True)
        self.output_files = {}
        self.clobber = clobber

        self.R1 = None
        self.i7 = None
        self.i5 = None
        self.R2 = None

        self.max_mismatches_per_index = max_mismatches_per_index

        self.bc_len = len(self.sample_map.keys()[0][0])
        for bc in self.sample_map.keys():
            if (len(bc[0]) != self.bc_len) or (len(bc[1]) != self.bc_len):
                raise ValueError('barcode lengths are not equal')

        self.i7_possible = set([k[0] for k in self.sample_map.keys()])
        self.i5_possible = set([k[1] for k in self.sample_map.keys()])

    def read_next_cluster(self):
        self.R1, self.i7, self.i5, self.R2 = self.iterator.next()

    def open_output_files(self):
        """

        :return:
        """
        for sample_name in self.sample_map.itervalues():
            self.output_files[sample_name] = (
                safeOFW("%s_fwd.fastq" % sample_name, clobber=self.clobber),
                safeOFW("%s_rev.fastq" % sample_name, clobber=self.clobber))

    def close_output_files(self):
        """

        :return:
        """
        for r1_file, r2_file in self.output_files.itervalues():
            r1_file.close()
            r2_file.close()

    def current_sample(self):
        """

        :return:
        """

        current_i7 = None
        current_i5 = None

        # check for exact i7 match
        if self.i7[:self.bc_len] in self.i7_possible:
            current_i7 = self.i7[1][:self.bc_len]
        else:  # check for acceptable not exact match
            for p in self.i7_possible:
                mismatches = sum(
                    [p[i] != self.i7[1][i] for i in range(self.bc_len)])
                if mismatches <= self.max_mismatches_per_index:
                    current_i7 = p
        if not current_i7:
            # no i7 match
            return None

        # check for exact i5 match
        if self.i5[:self.bc_len] in self.i5_possible:
            current_i5 = self.i5[1][:self.bc_len]
        else:
            for p in self.i5_possible:
                mismatches = sum(
                    [p[i] != self.i5[1][i] for i in range(self.bc_len)]
                )
                if mismatches <= self.max_mismatches_per_index:
                    current_i5 = p
        if not current_i5:
            return None

        if (current_i7, current_i5) not in self.sample_map:
            return None

        return self.sample_map[(current_i7, current_i5)]

    def write_R1_R2_sequences(self):
        """

        :return:
        """
        assigned_sample = self.current_sample()
        if assigned_sample:
            print >> self.output_files[assigned_sample][0], FASTQ_STRING_FMT % self.R1
            print >> self.output_files[assigned_sample][1], FASTQ_STRING_FMT % self.R2

    def process_set(self):
        """

        :return:
        """
        self.open_output_files()
        try:
            while True:
                self.read_next_cluster()
                self.write_R1_R2_sequences()
        except StopIteration:
            pass
        finally:
            self.close_output_files()

    @classmethod
    def rename_paired_files(cls, mapping_file, rename_with_column="Name"):
        from ubiota_data.mapping import MappingFile

        stub_names = lambda stub_list: flatten(
            [('%s_1.fastq' % name, '%s_2.fastq' % name) for name in stub_list])

        mf = MappingFile(mapping_file)

        old_stubs = mf.keys()
        new_stubs = [mf[k][rename_with_column] for k in old_stubs]

        rename_pairs = zip(stub_names(old_stubs), stub_names(new_stubs))

        for old, new in rename_pairs:
            os.rename(old, new)
