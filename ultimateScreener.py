#!/usr/local/bin/python -u

import os,sys,os.path
import shutil
import commands
import time
from utils.timeprofile import TimeProfiler
from sequence import fasta,screen
from types import *
#from ncbi.giInfo import GiInfo
import grid
from grid import rpyc
import glob
import sequence
import xmlParser as X
import time

print 'starting program...'
cushion=15
#^define your shortylength and cushion size here
#shortylength is designed to be twice the cushion
serverWait=60
#define how long the program will wait for open grid servers

compute0IP='10.11.12.53'
compute1IP='10.11.12.51'
compute2IP='10.11.12.52'

def biggestRemoteRecord(servers):
    """return the largest remote record
    """
    nMax = 0
    winner = None
    for i,s in enumerate(servers):
        mid1=time.time()
        n = s.execute('screener.longestRemaining[-1][-1]','eval')
        mid2=time.time()
        print 'mid2  time: %s'%(mid2-mid1)
        if n > nMax:
            nMax = n
            winner = i
    l=servers[winner].execute('screener.longestRemaining','eval')
    return l
    



sliceNumbers=[]
for t in glob.glob('slice*.fasta'):
    sliceNumbers.append(t.split('.')[0].split('e')[1])
serverCt=len(sliceNumbers)

servers = rpyc.GridServers(serverCt,verbose=True,timeout=serverWait)
print 'servers done'
screeners = []
setups = []
requests = []

for s in servers:
    s.execute("from sequence import xmlParser as X")
    s.execute("screener=X.UltimateIterativeScreen(cushion=%s)"%cushion)
    setups.append(rpyc.Async(s.namespace.screener.setup))
    screeners.append(rpyc.Async(s.namespace.screener.screen))

for i,s in enumerate(setups):
    requests.append(s(sliceNumbers[i]))

print 'setting up slices...'
while False in [r.is_ready for r in requests]:
    time.sleep(1)
print "Longest Base Counts:",[r.result for r in requests]


#Main Loop coming up

cycle = 0
oFile=file('ultimateScreener.out','w')
longest = biggestRemoteRecord(servers)
while longest[-1][-1]>0:
    start=time.time()
    print >> oFile, longest
    
    requests=[]

    for i,s in enumerate(screeners):
        #submit screening job
        requests.append(s(longest[0]))
                            
    while [r.is_ready for r in requests].count(True)<serverCt:
        time.sleep(0.01)
        
    print "Longest Base Counts:",[r.result for r in requests]


    cycle+=1
    print "cycle ", cycle, " done"
    longest = biggestRemoteRecord(servers)
    print longest

    end=time.time()
    print 'cycle time: %s'%(end-start)

oFile.close()


