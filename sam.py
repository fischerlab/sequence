"""SAM/BAM Utilities that are free of DB connections.
"""
__version__ = tuple([int(ver) for ver in
                     "$Revision: 1.3 $".split()[1].split('.')])
__author__ = "Kael Fischer"

import sys
import types
import operator
from __init__ import *
from utils import *
from utils import pipeline
from utils.decorators import *
import fastq

class FlagError (Exception):
    pass

# SAM TUPLE indices
QNAME=0
FLAG=1
RNAME=2
POS=3
MAPQ=4
CIGAR=5
RNEXT=6
PNEXT=7
TLEN=8
SEQ=9
QUAL=10
TAGS=11


def samTupleGenerator(samFile,parseTags=False,skipBits=0):
    """Iterator over sam result alignments:
    
    (qName,flag,rName,pos,mapQ,cigar,rNext,pNext,tLen,seq,qual,tags)

    see: http://samtools.sourceforge.net/SAM1.pdf
    
    Skips comments and common SGE lines of output. 

    If parseTags is True, a dictionaty of optional tags is returned,
    otherwise the unparsed tag string is returned in the tags slot.
    
    """
    
    lineN=0
    for l in multiFile(samFile):
        lineN+=1
        f=l.strip().split('\t')
        try:
            (qName,flag,rName,pos,mapQ,cigar,
             rNext,pNext,tLen,seq,qual)   =  f[:TAGS]
            flag=int(flag)
            if skipBits & flag != 0:
                continue
            if not parseTags:
                tags = '\t'.join(f[TAGS:])
            else:
                tags={}
                for opt in f[11:]:
                    tag,typ,val = opt.split(':')
                    if typ in ('AZ'):
                        pass
                    elif typ=='i':
                        val = int(val)
                    elif typ=='f':
                        val = float(val)
                    elif val == 'H':
                        #hex byte array
                        pass
                    elif val == 'B':
                        # other numeric array
                        pass
                    tags[tag]=val
                
            
        except ValueError:
            if (True or l.startswith('@') or l.startswith('#') or
                l.startswith('Reported ') or
                l.startswith('Warning:') or
                l.startswith('Thus no job control in this shell.')):
                continue
            else:
                sys.stderr.write('parsing error on line %d:\n\t%s\n'%
                                 (lineN,l))
                raise 

        flag,pos,mapQ,pNext,tLen = map(int,(flag,pos,mapQ,pNext,tLen))

        yield (qName,flag,rName,pos,mapQ,cigar,
               rNext,pNext,tLen,seq,qual,tags) 


def coverageVector(samTuples,vectorLength):
    import numpy as N
    v=N.zeros(vectorLength,int)
    for st in samTuples:
        v[st[POS]:st[POS]+len(st[SEQ])]+=1

    return v

def _extractFlag(flag):
    """
    """
    if type(flag) == IntType:
        return flag
    elif type(flag) in (types.TupleType, types.ListType):
        if type(flag[FLAG]) == IntType:
            return flag[FLAG]
    else:
        try:
            return int(flag)
        except ValueError:
            raise FlagError, "Flag unreadable: %s" %flag

def allSet(flag,*bits):
    """
    """
    myBits=reduce(operator.or_,(int(b) for b in bits))
    return (myBits & _extractFlag(flag)) == myBits

def anySet(flag,*bits):
    """
    """
    myBits=reduce(operator.or_,(int(b) for b in bits))
    return (myBits & _extractFlag(flag)) > 0

def onlySet(flag,*bits):
    """
    """
    myBits=reduce(operator.or_,(int(b) for b in bits))
    return (myBits ^ _extractFlag(flag)) == 0


class Flag (object):
    """
    """
    def __init__ (self,description,*bits):
        """
        """
        self.bits=reduce(operator.or_,(int(b) for b in bits))
        self.description=description

    def __str__(self):
        """
        """
        return self.description

    def isSet (self,flag):
        """
        """
        return (self.bits & _extractFlag(flag))==self.bits

    def exactMatch(self,flag):
        """
        """
        return (self.bits ^ _extractFlag(flag)) == 0

    def notSet(self,flag):
        """
        """
        return not self.isSet(flag)

    def __int__ (self):
        """
        """
        return self.bits

    def __or__ (self,other):
        """
        """
        return self.bits | int(other)

    def __and__ (self,other):
        """
        """
        return self.bits & int(other)

    def __xor__(self,other):
        """
        """
        return self.bits ^ int(other)
    

secondaryFlag=Flag('secondary alignment',0x100)
qualityFlag=Flag('not passing quality controls',0x200)
suppFlag=Flag('supplementary alignment',0x800)
dupFlag=Flag('PCR or optical duplicate',0x400)
firstFlag=Flag('first segment in the template',0x40)
lastFlag=Flag('last segment in the template',0x80)
rcFlag=Flag('alignment reverse complemented',0x10)
mateRcFlag=Flag('mate reverse complemented',0x20)
pairFlag=Flag('part of pair',0x1)
pairAlignedFlag=Flag('both mates aligned',0x2)
mateUnmappedFlag=Flag('mate unmapped',0x8)
readUnmappedFlag=Flag('segment unmapped',0x4)

singleFlags=tuple(
    sorted([secondaryFlag,
            qualityFlag,
            suppFlag,
            dupFlag,
            firstFlag,
            lastFlag,
            rcFlag,
            mateRcFlag,
            pairFlag,
            pairAlignedFlag,
            mateUnmappedFlag,
            readUnmappedFlag,],
           key=lambda x: x.bits))

flagValues=lambda f: [int(x.isSet(f)) for x in singleFlags]


whatFlags=lambda f: [x for x in singleFlags if x.isSet(f)]

def flagString(flag, delimiter='\n'):
    """
    """
    return delimiter.join((str(x) for x in whatFlags(flag)))

def isPrimary(flag):
    """
    """
    return secondaryFlag.notSet(flag)

def isSecondary(flag):
    """
    """
    return secondaryFlag.isSet(flag)

def isSupplementary(flag):
    """
    """
    return suppFlag.isSet(flag)

def isDuplicate(flag):
    """
    """
    return dupFlag.isSet(flag)

def isPaired(flag):
    """
    """
    return pairFlag.isSet(flag)

def bothMapped(flag):
    """
    """
    return pairAlignedFlag.isSet(flag)

def isUnmapped(flag):
    """
    """
    return readUnmappedFlag.isSet(flag)

def failQuality(flag):
    """
    """
    return qualityFlag.isSet(flag)

def isMateUnmapped(flag):
    """
    """
    return mateUnmappedFlag.isSet(flag)

def isRC(flag):
    """
    """
    return rcFlag.isSet(flag)

def isMateRC(flag):
    """
    """
    return mateRcFlag.isSet(flag)

def isFirst(flag):
    """
    """
    return firstFlag.isSet(flag)

def isLast(flag):
    """
    """
    return lastFlag.isSet(flag)


def pileupTupleGenerator (pileupFile,parseTags=False,skipBits=0,storeLines=False):
    """Iterator over samtools pileups

    Returns tuple:
    (reference sequence name,reference position, reference base identity,
    reads,mappings)

    see: http://samtools.sourceforge.net/samtools.shtml#3
    
    Skips comments and common SGE lines of output. 

    """
    
    lineN=0
    for l in multiFile(pileupFile):
        lineN+=1
        f=l.strip().split('\t')
        readData={}
        mappingData={'starts':[],
                     'ends':0 }
        dels=[]
        insertions=[]
        #print l
        try:
            (refName,refPos,refBase,readCount,reads,qAscii)=  f
            qual = fastq.qualToInt(qAscii)
            iB=0
            iQ=0
            while True:
                # todo indels#
                try:
                    b=reads[iB]
                except IndexError:
                    break
                if b == '^':
                    iB+=1
                    q=fastq.qualToInt([reads[iB]])[0]
                    mappingData['starts'].append(q)
                elif b == '$':
                    mappingData['ends']+=1
                elif b=='-':
                    iB+=1
                    digits=[reads[iB]]
                    while reads[iB+1].isdigit():
                        iB+=1
                        digits.append(reads[iB])
                    dels.append(int(''.join(digits)))
                elif b=='+':
                    iB+=1
                    digits=[reads[iB]]
                    while reads[iB+1].isdigit():
                        iB+=1
                        digits.append(reads[iB])
                    iLen=int(''.join(digits))
                    iB+=1
                    iSeq=reads[iB:iB+iLen]
                    iB+= (iLen-1)
                    insertions.append((iLen,iSeq))
                else:
                    q= fastq.qualToInt([reads[iQ]])[0]
                    iQ+=1
                    try:
                        readData[b].append(q)
                    except KeyError:
                        readData[b]=[q]
                iB+=1

        except ValueError:
            if (True or l.startswith('@') or l.startswith('#') or
                l.startswith('Reported ') or
                l.startswith('Warning:') or
                l.startswith('Thus no job control in this shell.')):
                continue
            else:
                sys.stderr.write('parsing error on line %d:\n\t%s\n'%
                                 (lineN,l))
                raise 

        refPos,readCount = map(int,(refPos,readCount))
        if storeLines:
            yield (refName,refPos,refBase,readCount,readData,
                   mappingData,insertions,dels,l)
        else:
            yield (refName,refPos,refBase,readCount,readData,
                   mappingData,insertions,dels)


def  readCounts(pileUpTuple,qualityCutoff=30,normalize=False):
    """
    """
    ref=pileUpTuple[2]
    cts={}

    for k,v in pileUpTuple[4].iteritems():
        if k in '.,':
            k=ref
        if k not in cts:
            cts[k.upper()]=0
        cts[k.upper()]+=sum([x>=qualityCutoff for x in v])

    if normalize:
        total=float(sum(cts.values()))
        ctList=[(v/total,k) for k,v in cts.items()]
    else:
        ctList=[(v,k) for k,v in cts.items()]
    ctList.sort()
    return ctList

def vcfTupleGenerator(vcfFile):
    """CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	
    """
    lineN=0
    for l in multiFile(vcfFile):
        #print lineN
        f=l.strip().split('\t',9)
        try:
            (chrom,pos,vid,ref,alt,qual,filter,info,format,samples)=  f
            pos=int(pos)
            alt=alt.split(',')
            qual=float(qual)
            iParts=info.split(';')
            info={}
            for i in iParts:
                if "=" in i:
                    k,v = i.split('=')
                    if  '.' in v or 'e' in v:
                        v=[float(x) for x in v.split(',')]
                    else:
                        v=[int(x) for x in v.split(',')]

                    if len(v) == 1:
                        v=v[0]
                    info[k]=v                

                else:
                    info[k]=None

                        
            samples = [[ int(x) for x in y.split(',')] for y in samples.split() ]

        except ValueError:
            if ( l.startswith('@') or l.startswith('#') or
                l.startswith('Reported ') or
                l.startswith('Warning:') or
                l.startswith('Thus no job control in this shell.')):
                continue
            else:
                sys.stderr.write('parsing error on line %d:\n\t%s\n'%
                                 (lineN,l))
                raise 


        yield (chrom,pos,vid,ref,alt,qual,filter,info,format,samples)
        lineN+=1
        

def vcfCall(vcfTuple,ambiguousLog=None):
    """
    """
    t=vcfTuple
    if 'DP4' in t[7]:
        # -v variant only style
        dp4=t[7]['DP4']
        ref=t[3]
        var=t[4][0]
        ctVar = sum(dp4[-2:])
        ctRef = sum(dp4[:2])
                    
        pctVar = float(ctVar)/sum(dp4)
        pctRef = float(ctRef)/sum(dp4)

        if pctRef > pctVar:
            if pctRef < 0.9:
                call=vcfTuple[3].lower()
                if ambiguousLog is not None:
                    print >> ambiguousLog, "%s\t%s(%0.1f)\t%s(%0.1f) called refernce %s" %(
                        t[1],ref,pctRef*100,var,pctVar*100,ref)
            else:
                call=vcfTuple[3]
        else: #pctVar> pctRef:
            if pctVar < 0.9:
                call=vcfTuple[4][0].lower()
                if ambiguousLog is not None:
                    print >> ambiguousLog, "%s\t%s(%0.1f)\t%s(%0.1f)\tcalled variant %s" %(
                       t[1],ref,pctRef*100,var,pctVar*100,var)
            else:
                call=vcfTuple[4][0]
        return call
           
        

def vcfMutantGenerator(vcfFile,ambiguousLog=None):
    """
    """
    for t in vcfTupleGenerator(vcfFile):
        yield (t[1],vcfCall(t,ambiguousLog))
    
               



class PileOn (list):
    """
    """
    


class SamPipelineJob (pipeline.Job):
    """
    """

class SamsToBams (pipeline.Step):
    """samtools view -b
    """
    outputGlob='%s.bam'
    gridItCmd="samtools view -Sb -o %s.bam %s"
    def gridItArgs(self,slices):
        gia = (['-N ' + self.gridItName(),
                self.gridItCmd] +
               [' '.join((x,x)) for x in self.inputFiles(slices)])
        return gia


class CatBams (pipeline.SingleOutputStep):
    """samtools cat
    """


class MergeSortedBams (pipeline.SingleOutputStep):
    """samtols merge
    """

class IndexBams (pipeline.SingleOutputStep):
    """samtools index
    """
    outputGlob='%s.sort.bam.bai'
    gridItCmd="samtools index"
    

class BamSort (pipeline.SingleOutputStep):
    """samtolls index
    """
    outputGlob='%s.sort.bam'
    gridItCmd="samtools sort -b %s %s.sort"
    def gridItArgs(self,slices):
        gia = (
            ['-N ' + self.gridItName(),
             self.gridItCmd] +
            [' '.join((x,os.path.splitext(x)[0]))
             for x in self.inputFiles(slices)])
        return gia

class MakePileup (pipeline.SingleOutputStep):
    """samtols index
    """

    outputGlob='%s.pileup'
    gridItCmd="samtools mpileup -B %s %%s \> %%s.pileup"

    def gridItArgs(self,slices):
        if self.job.opts.reference is not None:
            refStr = '-f %s' % self.job.opts.reference
        else:
            refStr=''
        gia = (['-N %s' % self.gridItName(),self.gridItCmd % refStr] +
               [' '.join((x,os.path.splitext(os.path.splitext(x)[0])[0]))
                for x in self.inputFiles(slices)])
        return gia
    
    

    def inputFiles(self, slices = None):
        try:
            sStep=self.job['sort']
        except KeyError:
            sStep.BamSort('sort')
            sStep.job=self.job
        rv= sStep.outputFiles()

        if slices == None:
            return rv
        else:
            rv2 = []
            for n in slices:
                rv2.append(rv[n])
            return sorted(rv2)


class MakeBcf (MakePileup):
    """samtols mpileup -g
    """
    outputGlob='%s.bcf'
    gridItCmd="samtools mpileup -g -B %s %%s \> %%s.bcf"
    
    

    


classReference = {
    'sam2bam':SamsToBams,
    'catbam':CatBams,
    'mergeBam':MergeSortedBams,
    'indexBam':IndexBams,
    'sort':BamSort,
    'index':IndexBams,
    'pileup': MakePileup,
    'makeBcf':MakeBcf,
    }
