"""code for dealing with aligned fasta
numpy (unsigned 8bit) representation is used:

row = seq.rec
col = aligned pos (with gaps starting from 0

-=0
A=1
C=2
G=4
T=8

"""
import fasta as F
import numpy as N

import operator

base = {0: '-',
        1: 'A',
        2: 'C',
        3: 'M',
        4: 'G',
        5: 'R',
        6: 'S',
        7: 'V',
        8: 'T',
        9: 'W',
        10: 'Y',
        11: 'H',
        12: 'K',
        13: 'D',
        14: 'B',
        15: 'N',
        '-': 0,
        'A': 1,
        'B': 14,
        'C': 2,
        'D': 13,
        'G': 4,
        'H': 11,
        'I': 11,
        'K': 12,
        'M': 3,
        'N': 15,
        'R': 5,
        'S': 6,
        'T': 8,
        'V': 7,
        'W': 9,
        'Y': 10}


def rowsOr(mat):
    """or the rows of a matrix
    """
    return reduce(operator.__or__,[mat[i,:] for i in range(mat.shape[0])])

def rowVec2Str(row):
    """
    """
    t=[]
    for n in row:
        t.append(base[n])
    return ''.join(t)

def oneIfFound(row,n):
    """
    """
    return (row & n >0).astype('B')



class Alignment (dict):
    """Holds aligned sequences
    """


    def __init__ (self,inFasta):
        self.order=[]
        for s in F.FastaIterator(inFasta):
            self.order.append(s.title)
            self[s.title]=s
        
        self.alnLength = max([len(s) for s in self.values()])
        self.seqMat=N.zeros((len(self),self.alnLength),
                            dtype='B')
        for i,k in enumerate(self.order):
            for j,b in enumerate(self[k]):
                self.seqMat[i,j]=base[b]

        self.seqOr=rowsOr(self.seqMat)


    def posDiversity(self):
        return reduce(operator.__add__, [oneIfFound(self.seqOr,base[c])
                                         for c in 'ACGT'])
    
            
    def orAllSeq(self,keys=None):
        pass

    def baseComp(self,start=None,end=None):
        rv={}
        
        
    
