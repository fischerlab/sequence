#!/usr/local/bin/python
#
# Splits a fasta or fastq file.
#
__version__ = tuple([int(x) for x in
                     '$Revision: 1.2 $'.split()[1].split('.')])
__author__ = "Julio Menendez"

import sys, os
from optparse import OptionParser

from sequence import fasta

def main(args):
    parser = OptionParser('usage: %prog [options]')
    optionsAvailable = (
        ('-i', '--input', 'inputfile', 'string', 'Input file'),
        ('-r', '--maxrec', 'maxRec','int', 'Maximum size of each record'),
        ('-c', '--numrec', 'numRec', 'int', 'Number of parts to generate'),
        )
    for oa in optionsAvailable:
        parser.add_option(oa[0], oa[1], dest=oa[2], type=oa[3], help=oa[4])
    
    (options, args) = parser.parse_args(args)

    if options.inputfile is None:
        print >> sys.stderr, '-i input file name is required'
        print >> sys.stderr, parser.format_help()
        sys.exit(1)
        
    if options.maxRec is None and options.numRec is None:
        print >> sys.stderr, 'One of -r or -c is required'
        print >> sys.stderr, parser.format_help()
        sys.exit(1)

    if options.maxRec:
        recCount = fasta.fastaCount(options.inputfile)
        count = (recCount / options.maxRec +
                 int(recCount % options.maxRec != 0))
    else:
        count = int(options.numRec)

    ng = nameGenerator(options.inputfile, count)

    fasta.splitFasta(options.inputfile, splitCt=count,
                     nameGenerator=ng)
    sys.exit(0)

def nameGenerator(rootName, maxN):
    n=0
    fmt = '_%%0%dd' % int(ceil(log10(maxN)))
    base,ext = os.path.splitext(rootName)
    while True:
        name=base+ fmt% (n) + ext
        yield file(name,'w'),name
        n+=1

if __name__ == '__main__':
    main(sys.argv[1:])
