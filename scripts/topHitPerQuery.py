#!/usr/local/lib/python

from sequence import blastNoSQL
from utils import readArgsOrFiles

import sys
import os


def extractHSPs(inFile):
    rv={}
    for m8rec in blastNoSQL.m8generator(inFile):
        if m8rec['query'] not in rv:
            rv[m8rec['query']]=[m8rec]
        elif  m8rec['score'] > rv[m8rec['query']][0]['score']:
            rv[m8rec['query']]=[m8rec]
        elif m8rec['score'] == rv[m8rec['query']][0]['score']:
            rv[m8rec['query']].append(m8rec)

    kList = rv.keys()
    kList.sort()
    for k in kList:
        for m8 in rv[k]:
            print m8['_str_']
    


if __name__ == "__main__":
    args = sys.argv[1:]
    extractHSPs(readArgsOrFiles(args))

    

    
