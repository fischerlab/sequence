#!/usr/bin/env python
#
#
__version__ = (0, 1)
__author__ = "Kael Fischer"

import optparse
import sys

from ubiota_data import mapping

from sequence.fastq import SplitMiSeqSet


def main(sysargs=[]):
    # short usage message
    oneLineUsage = "Usage: %prog <mapping file> <fastq_1> <fastq_2> <fastq_3> <fastq_4>"

    # set a long description for the help message
    description = ("Split a 4 fastq file set using a QIMME-type mapping file "
                   "into per-sample (per-barcode) fastq files.  Two files "
                   "persample are created, of the form: <sample_ID>_fwd.fastq and "
                   "<sample_ID>_rev.fastq containing matching records from fasta_1 "
                   "and fasta_4, respectively.")

    op = optparse.OptionParser(
        oneLineUsage, description=description,
        version="%prog " + '.'.join([str(x) for x in __version__]))

    # OPTION DEFINITIONS
    op.add_option('-c', '--clobber', action="store_true", default=False,
                  dest="clobber", help="allow output file to be overwritten.")
    op.add_option('-M', '--mismatches-allowed-per-index', action="store", type="int",
                  default=1, dest="max_mismatch",
                  help="the allowed number of mismatches in each index.")
    op.add_option('-n', '--name', action="store", type="str",
                  default='', dest="name_field",
                  help="mapping field to use for file naming. "
                       "By default the first column is used.")

    # all options should be defined above here
    # (you don't have to define version or help)

    # add defaults to help messages     
    for o in op.option_list:
        if o.type == None:
            continue
        if o.help == None:
            o.help = "Default: %default"
        else:
            o.help += " Default: %default"

    # OPTION PARSING                    
    (opts, args) = op.parse_args(sysargs)

    # ARGUMENT CHECKING
    # something like
    try:
        if len(args) != 5:
            raise RuntimeError, "There must be exactly 5 arguments."
    except Exception, eData:
        print >> sys.stderr, ("\nUsage Error: %s\n" % eData.message)
        print >> sys.stderr, op.format_help()
        return 1

    rv = extract_pairs(opts, args)

    return (rv)  # we did it!


#
# end of main
#

def extract_pairs(opts, args):
    """

    :param opts:
    :param args:
    :return:
    """
    mapping_fp, fq_1, fq_2, fq_3, fq_4 = args

    # make custom mapping structure
    sample_map = {}
    mf = mapping.MappingFile(mapping_fp)
    for samp_id, samp_data in mf.iteritems():
        if opts.name_field:
            samp_id = samp_data[opts.name_field]

        i7 = samp_data['BarcodeSequence'][:8]
        i5 = samp_data['BarcodeSequence'][-8:]
        sample_map[(i7, i5)] = samp_id

    opt_kdws = {'clobber': opts.clobber,
                'max_mismatches_per_index': opts.max_mismatch}

    splitter = SplitMiSeqSet(sample_map, fq_1, fq_2, fq_3, fq_4, **opt_kdws)
    splitter.process_set()

    return 0


####### LEAVE THIS ALONE ###########
# If run directly as a program
# this calls the main function.
# 
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
