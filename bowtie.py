"""Bowtie Utilities.
"""
__version__ = tuple([int(ver) for ver in
                     "$Revision: 1.1 $".split()[1].split('.')])

__author__ = "Kael Fischer"
from types import StringTypes
from blastNoSQL import giFromM8name

def l2t (line):
    """parse a line to a low overhead  tuple, where
    the elements have the correct data type
    """
    (readName,direction,refName,position,
     readSeq,readQuals,iaCount,mmTxt) =  line.strip().split('\t')

    position=int(position)
    mm = tuple(mmTxt.split(','))


    return (readName,direction,refName,position,
            readSeq,readQuals,iaCount,mm)

def readTitleSet(rsltPaths,qrySet=False,subjSet=False):

    """return a set of query titles present in
    one or more bowtie alignment files
    """
    if type(rsltPaths) in StringTypes:
        rsltPaths = [rsltPaths,]
    if not qrySet and not subjSet:
        raise ValueError, "Either qrySet or subjSet must be True"
   
    rv=set()
    idxs=[]
    if qrySet:
        idxs.append(0)
    if subjSet:
        idxs.append(2)
        
    for btFile in rsltPaths:
        for l in file(btFile):
            for i in idxs:
                rv.add(l.split('\t')[i])
    return rv

